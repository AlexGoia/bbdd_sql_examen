/* 

VOLVER A HACER EL EXAMEN
FECHA DE ENTREGA: 9 DE ABRIL DE 2017
�LEX GOIA

 */

--1- Saca los �rbitros y el nombre los jugadores que hayan jugado de delantero (DL) o centrocampista
--(MC), en partidos donde su equipo ha jugado de local y ganado por m�s de 1 gol en el Mundial 2006 o
--2010. Quitando duplicados y que salgan en orden cronol�gico de esos partidos, no se pueden usar
--conjuntos. (0,75 pto)
  
SELECT ART.NOMBRE_ARB, JGR.NOMBRE_JUG
FROM JUGADOR JGDR, JUGAR JGR, PARTIDO PRT, ARBITRAR ART
-- CRUZAMOS
WHERE JGDR.NOMBRE=JGR.NOMBRE_JUG
AND JGR.EQUIPO_L_PART=PRT.EQUIPO_L
AND JGR.EQUIPO_V_PART=PRT.EQUIPO_V
AND JGR.FECHA_PART=PRT.FECHA
-- CONDICIONES
AND (JGR.PUESTO_JUGAR='DL' OR JGR.PUESTO_JUGAR='MC')--QUE HAYA JUGADO COMO DL O MC
AND JGR.EQUIPO_L_PART=JGDR.EQUIPO_JUGADOR--QUE SU EQUIPO HAYA JUGADO DE LOCAL
AND (PRT.RESULTADO_L-PRT.RESULTADO_L)>=1-- QUE HAYA GANADO POR M�S DE 1
AND (TRUNC(PRT.FECHA,'YYYY'))IN TO_DATE('2006','YYYY')-- QUE SEA DEL 2006

--2- Haz una select de todos los datos de los jugadores que hayan jugado entre tres y cinco partidos, que
--en ninguno de ellos haya jugado el partido completo (90 minutos) y que no hayan marcado ning�n
--GOL debes usar al menos una vez EXISTS y no se puede usar el operador >=. (1,5 ptos)
SELECT *
FROM JUGADOR JGD
WHERE JGD.NOMBRE IN (
SELECT JGR.NOMBRE_JUG
FROM JUGAR JGR
WHERE JGR.MIN_JUGAR<=90 -- QUE HAYA JUGADO MENOS DE 90 MIN
AND NOT EXISTS (-- QUE NO HAYA MARCADO NING�N GOL
    SELECT 1
    FROM GOL GL
    WHERE GL.EQUIPO_L_GOL=JGR.EQUIPO_L_PART
    AND GL.EQUIPO_V_GOL=JGR.EQUIPO_V_PART
    AND GL.FECHA_GOL=JGR.FECHA_PART
    AND GL.JUGADOR_GOL=JGR.NOMBRE_JUG
)
GROUP BY JGR.NOMBRE_JUG
HAVING COUNT(*) BETWEEN 3 AND 5
)

--3- Obt�n la selecci�n que menos goles ha marcado en todos los Mundiales, indistintamente si el
--partido lo jug� de local o de visitante, debe aparecer junto a dicha selecci�n, los goles totales
--marcados y el n�mero de partidos que jug� para marcarlos (2,5 ptos).
--NOTA: Recuerda que los goles marcados por el EQUIPO_L est�n en RESULTADO_L y del
--EQUIPO_V en RESULTADO_V.
SELECT DECODE(EQ.EQUIPO,PRT.EQUIPO_L,PRT.EQUIPO_L,PRT.EQUIPO_V) NOMBRE_EQUIPO, COUNT(*) PARTIDOS_JUGADOS, SUM(DECODE(EQ.EQUIPO,PRT.EQUIPO_L,PRT.RESULTADO_L,PRT.RESULTADO_V)) GOLES_MARCADOS
FROM PARTIDO PRT, EQUIPOS EQ
WHERE PRT.EQUIPO_L=EQ.EQUIPO -- CRUZAMOS
OR PRT.EQUIPO_V=EQ.EQUIPO
GROUP BY DECODE(EQ.EQUIPO,PRT.EQUIPO_L,PRT.EQUIPO_L,PRT.EQUIPO_V)
HAVING SUM(DECODE(EQ.EQUIPO,PRT.EQUIPO_L,PRT.RESULTADO_L,PRT.RESULTADO_V))=(
    SELECT MIN(GOLES_MARCADOS)
    FROM (
    SELECT DECODE(EQ.EQUIPO,PRT.EQUIPO_L,PRT.EQUIPO_L,PRT.EQUIPO_V) NOMBRE_EQUIPO, COUNT(*) PARTIDOS_JUGADOS, SUM(DECODE(EQ.EQUIPO,PRT.EQUIPO_L,PRT.RESULTADO_L,PRT.RESULTADO_V)) GOLES_MARCADOS
    FROM PARTIDO PRT, EQUIPOS EQ
    WHERE PRT.EQUIPO_L=EQ.EQUIPO -- CRUZAMOS
    OR PRT.EQUIPO_V=EQ.EQUIPO
    GROUP BY DECODE(EQ.EQUIPO,PRT.EQUIPO_L,PRT.EQUIPO_L,PRT.EQUIPO_V)
    )
)

--4- Obt�n el nombre ordenado descendente de cada selecci�n juntos con los partidos del mundial 2010
--que ha ganado como local y con los partidos que ha perdido como visitante. No se puede usar
--conjuntos (2 ptos).
SELECT *
FROM EQUIPOS EQ, PARTIDO PRT
WHERE 
( -- QUE HA GANADO COMO LOCAL
    EQ.EQUIPO=PRT.EQUIPO_L
    AND PRT.RESULTADO_L>PRT.RESULTADO_V
    AND TO_CHAR(PRT.FECHA,'YYYY')='2010'
)-- HA GANADO COMO LOCAL
OR (
    EQ.EQUIPO = PRT.EQUIPO_V AND
    PRT.RESULTADO_V<PRT.RESULTADO_L
    -- QUE HA PERDIDO COMO VISITANTE
    AND TO_CHAR(PRT.FECHA,'YYYY')='2010'
)
ORDER BY EQ.EQUIPO DESC;

--5- Inserta 1 laboratorio y 1 federativo invent�ndote los datos. Para luego con este laboratorio y este
--federativo, inserta an�lisis antidoping para cada partido del Mundial, de modo que se har�n el control
--en cada partido todos aquellos jugadores que m�s minutos han jugado en cada partido o que hayan
--metido m�s de 1 gol en el mismo (se debe hacer en una sola insert). (2,5 ptos)
INSERT INTO LABORATORIO (CIF, NOMBRE)
VALUES('00001','NOTRAMPOSOS.SL');

INSERT INTO FEDERATIVO(ID_TARJ, NOMBRE)
VALUES('00001','QUETE PILLAO');

INSERT INTO ANALISIS (JUGADOR,EQUIPO_L,EQUIPO_V, FECHA_PART, CIF, ID_TARJETA)
    (
            SELECT JGR1.NOMBRE_JUG, JGR1.EQUIPO_L_PART, JGR1.EQUIPO_V_PART, JGR1.FECHA_PART, '00001', '00001'
            FROM JUGAR JGR1
            WHERE JGR1.MIN_JUGAR = (
                SELECT MAX(JGR.MIN_JUGAR)
                FROM JUGAR JGR
                WHERE JGR.EQUIPO_L_PART = JGR1.EQUIPO_L_PART AND -- DE ESE PARTIDO
                JGR.EQUIPO_V_PART = JGR1.EQUIPO_V_PART AND
                JGR.FECHA_PART = JGR1.FECHA_PART
                GROUP BY JGR.EQUIPO_L_PART, JGR.EQUIPO_V_PART, JGR.FECHA_PART
            ) -- EL JUGADOR QUE M�S HAYA JUGADO EN UN PARTIDO
            OR EXISTS ( 
                SELECT 1
                FROM GOL GL
                WHERE GL.EQUIPO_L_GOL=JGR1.EQUIPO_L_PART
                AND GL.EQUIPO_V_GOL =JGR1.EQUIPO_V_PART
                AND GL.FECHA_GOL=JGR1.FECHA_PART
                AND JGR1.NOMBRE_JUG=GL.JUGADOR_GOL
                GROUP BY JGR1.NOMBRE_JUG
                HAVING COUNT(*)>1 -- HAYA MARCADO M�S DE UN GOL EN UN PARTIDO DADO
            )
            GROUP BY JGR1.NOMBRE_JUG, JGR1.EQUIPO_L_PART, JGR1.EQUIPO_V_PART, JGR1.FECHA_PART -- EVITAMOS DUPLICADOS
            -- UEDE PASAR QUE JUEGUE EL QUE M�S Y MARQUE M�S DE DOS GOLES, LO QUE REPETIR�A EL PARTIDO POR EJEMPLO
    )
    
    
    
    
    
--6- Actualiza el puesto habitual de los jugadores con el puesto en el que mas veces haya jugado, en el
--caso que haya m�s de uno puesto donde haya jugado las mismas veces, cogeremos el alfab�ticamente
--mayor (2,5 ptos)

-- !! EST� BIEN, PERO TIENE UNA SOLUCI�N M�S SENCILLA !! --
UPDATE JUGADOR JGD SET
JGD.PUESTO_HAB = 
(
    SELECT MAX(PUESTO_JUGAR) -- DADO QUE PUEDE HABER JUGADO LA MISMA CANTIDAD DE VECES, SELECCIONAMOS EL M�XIMO
    FROM(
        SELECT JGR1.NOMBRE_JUG, JGR1.PUESTO_JUGAR, COUNT(*) 
        FROM JUGAR JGR1
        GROUP BY JGR1.NOMBRE_JUG, JGR1.PUESTO_JUGAR
        ORDER BY JGR1.NOMBRE_JUG
    ) PUESTOS_JUGADOS
    WHERE (PUESTOS_JUGADOS.NOMBRE_JUG, PUESTOS_JUGADOS."COUNT(*)") IN (
        SELECT TABLA.NOMBRE_JUG, MAX(TABLA.VECES_JUGADAS) MAX_VECES_JUGADAS
            FROM (
                SELECT JGR.NOMBRE_JUG, JGR.PUESTO_JUGAR, COUNT(*) VECES_JUGADAS
                FROM JUGAR JGR
                GROUP BY JGR.NOMBRE_JUG, JGR.PUESTO_JUGAR
                ORDER BY JGR.NOMBRE_JUG
            ) TABLA
            GROUP BY TABLA.NOMBRE_JUG
    )
    AND PUESTOS_JUGADOS.NOMBRE_JUG=JGD.NOMBRE -- CRUZAMOS CON EL PADRE
)
WHERE JGD.NOMBRE IN (
    SELECT NOMBRE_JUG
    FROM(
        SELECT JGR1.NOMBRE_JUG, JGR1.PUESTO_JUGAR, COUNT(*) 
        FROM JUGAR JGR1
        GROUP BY JGR1.NOMBRE_JUG, JGR1.PUESTO_JUGAR
        ORDER BY JGR1.NOMBRE_JUG
    ) PUESTOS_JUGADOS
    WHERE (PUESTOS_JUGADOS.NOMBRE_JUG, PUESTOS_JUGADOS."COUNT(*)") IN (
        SELECT TABLA.NOMBRE_JUG, MAX(TABLA.VECES_JUGADAS) MAX_VECES_JUGADAS
            FROM (
                SELECT JGR.NOMBRE_JUG, JGR.PUESTO_JUGAR, COUNT(*) VECES_JUGADAS
                FROM JUGAR JGR
                GROUP BY JGR.NOMBRE_JUG, JGR.PUESTO_JUGAR
                ORDER BY JGR.NOMBRE_JUG
            ) TABLA
            GROUP BY TABLA.NOMBRE_JUG
    )
)

--STAR WARS
--7- Obt�n de todos los personajes, que su profesi�n contenga una sola palabra, el n�mero de personajes
--que han reclutado, siendo 0 si no han resultado ninguno. (1,5 p)

-- SOL 2 --
SELECT NOMBRE, CONT PERSONAJES_RECLUTADOS
FROM(
    SELECT PE.NOMBRE_RECLUTADOR NOMBRE, COUNT(*) CONT
    FROM PERSONAJES PE
    WHERE INSTR(LTRIM(RTRIM(PE.PROFESION)), ' ')=0
    GROUP BY PE.NOMBRE_RECLUTADOR
) RECLUTADORES
WHERE EXISTS (
    SELECT 1
    FROM PERSONAJES PE2
    WHERE PE2.NOMBRE=RECLUTADORES.NOMBRE
    AND INSTR(LTRIM(RTRIM(PE2.PROFESION)), ' ')=0
)
UNION 
SELECT NOMBRE, 0
FROM PERSONAJES PE_N
WHERE INSTR(LTRIM(RTRIM(PE_N.PROFESION)), ' ')=0
AND NOT EXISTS (
    SELECT 1
    FROM (
        SELECT PE.NOMBRE_RECLUTADOR NOMBRE, COUNT(*) CONT
        FROM PERSONAJES PE
        WHERE INSTR(LTRIM(RTRIM(PE.PROFESION)), ' ')=0
        GROUP BY PE.NOMBRE_RECLUTADOR
    ) RECLUTADORES
    WHERE RECLUTADORES.NOMBRE=PE_N.NOMBRE
)
ORDER BY NOMBRE


--8- Personajes que hayan sido interpretados por Actores que no tengan un n�mero en su �ltima letra del
--nombre y que hayan hecho alg�n casting 
--o que hayan participado en menos de 2 actuaciones sin que
--ning�n fan haya hecho comentarios de las mismas 
--o que hayan sido m�s de 3 pero que su
--localizaciones tengan un coste superior a 10.000
-- y a todo esto qu�tale los SITH asesinos de otro SITH
--de mayor grado que ellos o que hayan sido un JEDI que haya sido convertido al lado obscuro. Debes
--usar al menos 1 vez EXISTS (3 ptos).
--(Nota: El SITH asesina a otro SITH lo tienes en la ternaria MATAR.
--Nota2: Recuerda que los campos de CONVERTIR est�n en JEDI)
SELECT NOMBRE
FROM PERSONAJES PE
WHERE EXISTS (
    SELECT ACT.NOMBRE, SUBSTR((ACT.NOMBRE),LENGTH(ACT.NOMBRE))
    FROM ACTORES ACT
    WHERE SUBSTR(
                RTRIM(ACT.NOMBRE)
                ,LENGTH(RTRIM(ACT.NOMBRE))
          ) NOT IN ('0','1','2','3','4','5','6','7','8','9')
    AND PE.NSS_ACTOR=ACT.NSS
)
INTERSECT -- Y
SELECT NOMBRE
FROM PERSONAJES PE
WHERE EXISTS ( -- QUE HAYA HECHO ALG�N CASTING
    SELECT 1
    FROM HACER_CASTING HC
    WHERE HC.NSS=PE.NSS_ACTOR
)
UNION -- O
SELECT NOMBRE
FROM PERSONAJES PE
WHERE EXISTS ( -- QUE HAYAN PARTICIPADO EN MENOS DE DOS ACTUACIONES
    SELECT 1
    FROM ACTUAR ACT
    WHERE ACT.NOM_PERSONAJE=PE.NOMBRE
    AND (ACT.COD_LOG,ACT.NOM_PERSONAJE, ACT.CAPITULO, ACT.TEMPORADA ) NOT IN (
            SELECT CODIGO_LOC, NOM_PERSONAJE, NUM_CAP, NUM_TEMPO
            FROM COMENTAR -- QUE NO HAYAN RECIBIDO COMENTARIOS
        )
    GROUP BY PE.NOMBRE
    HAVING COUNT(*)<2
)
UNION -- O
SELECT NOMBRE
FROM PERSONAJES PE
WHERE EXISTS ( -- QUE HAYAN PARTICIPADO EN MENOS DE DOS ACTUACIONES
    SELECT 1
    FROM ACTUAR ACT
    WHERE ACT.NOM_PERSONAJE=PE.NOMBRE
    AND ACT.COD_LOC IN (
        SELECT LOC.COD_LOC
        FROM LOCALIZACIONES LOC
        WHERE LOC.COSTE_LOC>10.000
    )
    GROUP BY PE.NOMBRE
    HAVING COUNT(*)>3
)
MINUS (
    SELECT NOMBRE
    FROM SITH SI, MATAR MT
    WHERE SI.NOMBRE_SITH=MT.SITH_ASESINO
    AND SI.GRADO_SITH< (
        SELECT GRADO_SITH
        FROM SITH
        WHERE NOMBRE_SITH=MT.SITH_ASESINADO
    )-- QUE HAYA MATADO A OTRO SITH Y SU GRADO SEA
    UNION
    SELECT NOMBRE
    FROM SITH SI
    WHERE SI.NOMBRE_SITH IN (
        SELECT CT.NOM_JEDI
        FROM CONVERTIR CT
    ) -- QUE HAYA SIDO CONVERTIDO DE JEDI A SITH
)

--9 Escribe cu�l seria el enunciado cuya soluci�n seria estos SQL (1 pto):
--Nota: Tu enunciado debe explicar el porque de cada cosa efectuada en el SQL
--(Pista: El resultado de un count nunca es NULL, m�nimo es 0.)


--AEROPUERTO
--10 Obt�n el n�mero de vuelo, el c�digo del avi�n y la capacidad de los aviones que m�s capacidad de
--pasajeros tienen por cada n�mero de vuelo, ord�nalos por n�mero de vuelo (2 ptos).

    SELECT DISTINCT VO1.NUM_VUELO, AV1.CODIGO, AV1.CAPACIDAD
    FROM AVIONES AV1, VOLAR VO1
    WHERE AV1.CODIGO=VO1.COD_AVION
    AND AV1.CAPACIDAD IN (
        SELECT MAX(AV.CAPACIDAD)
        FROM AVIONES AV, VOLAR VO
        WHERE AV.CODIGO=VO.COD_AVION
        AND VO1.NUM_VUELO=VO.NUM_VUELO -- POR NUMERO DE VUELO, DEPENDE DEL PADRE, POR ESO CRUZAMOS
        GROUP BY VO.NUM_VUELO
    )
    ORDER BY VO1.NUM_VUELO
    
--11 Obt�n los nombres de los aeropuertos que tengan una cantidad de despegues mayor que la media de
--despegues de todos los aeropuertos y su suma de plazas ocupadas de esos despegues sea mayor a 500.
--(2,25 ptos)
SELECT AE.NOMBRE, AE.NOMBRE_ALTERNATIVO
FROM AEROPUERTOS AE
WHERE AE.CODIGO IN (
    SELECT PV.COD_AERO_DESPEGAR
    FROM PROGRAMA_VUELO PV, VOLAR VO
    WHERE PV.NUM_VUELO = VO.NUM_VUELO
    GROUP BY PV.COD_AERO_DESPEGAR
    HAVING COUNT(*)>(
        SELECT ROUND(AVG(COUNT(*)),2) MEDIA_DESPEGUES -- LA MEDIA DE LOS DESPEGUES
        FROM PROGRAMA_VUELO PV2, VOLAR VO2
        WHERE PV2.NUM_VUELO=VO2.NUM_VUELO
        GROUP BY PV2.COD_AERO_DESPEGAR
        HAVING SUM(VO2.PLAZAS_OCUPADAS)>500 -- CONDICI�N QUE SE REPITE
    )
    AND SUM(VO.PLAZAS_OCUPADAS)>500 -- CONDICI�N QUE SE REPETIR� EN LA SELECT
)
ORDER BY AE.NOMBRE, AE.NOMBRE_ALTERNATIVO
    
--12 Crea una vista asegur�ndote en su creaci�n que no sea actualizable con todos los datos del
--programa de vuelo, del vuelo que m�s tarde vuele de la base de datos, en caso de haber m�s de uno
--obtendremos el de n�mero de vuelo mayor alfab�ticamente. (1,5 ptos)
CREATE VIEW VUELO_MAS_TARDIO
AS (
    SELECT *
    FROM PROGRAMA_VUELO 
    WHERE NUM_VUELO=(
            SELECT MAX(PV.NUM_VUELO)
            FROM PROGRAMA_VUELO PV, VOLAR VL
            WHERE PV.NUM_VUELO = VL.NUM_VUELO
            AND VL.FECHA_VUELO IN (
                SELECT MAX(VL2.FECHA_VUELO) 
                FROM PROGRAMA_VUELO PV2, VOLAR VL2
                WHERE PV2.NUM_VUELO = VL2.NUM_VUELO
            )
     )      
)WITH READ ONLY;

--13 Nuestro ya colega James Bond, y como conoces que t� tienes acceso a la base de datos de
--aeropuertos, nos pide que le ayudemos, nos informa que un peligroso esp�a chino se esta haciendo
--pasar por DIRECTOR de un aeropuerto, sabe que desde su aeropuerto en CHINA, que desconocemos,
--volar� con destino a un aeropuerto de USA, este vuelo no sabemos su fecha, solo sabemos que fue un
--14 de enero y que ese vuelo sale 3 d�as a la semana, y hemos podido averiguar parte de un n�mero del
--avi�n, tanto su c�digo como el modelo tienen tres 7.
--Nuestro agente necesita saber la fecha del vuelo, el n�mero de vuelo, el nombre de los aeropuertos de
--origen y destino (en este orden), el modelo del avi�n y los m�s importantes el nombre del esp�a, quiere
--que aparezca ESPIA en el encabezado del resultado de ese campo. Es un asunto de seguridad nacional,
--no lo cuentes por ah�, si lo haces bien le dir� a tu profe que te suba (1 punto) en el examen.
